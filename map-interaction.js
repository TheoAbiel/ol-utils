/**
 * Determine if the mouse is over a certain layer
 * @param  {ol.Map}  map
 * @param  {ol.MapBrowserEvent}  evt
 * @param  {string}  layerName
 * @return {boolean}
 */
function isMouseOver(map, evt, layerName) {
    var feature = map.forEachFeatureAtPixel(evt.pixel,
            function (feature, layer) {
                return feature;
            },
            null,
            function (layer) {
                return layer === vectorLayer;
            });

    if (feature) {
        return true;
    }

    return false;
}
