/**
 * Center the map to location
 * @param  {ol.Map} map
 * @param  {float} x
 * @param  {flaot} y
 * @return {undefined}
 */
function goToCoord(map, x, y) {
    var view = map.getView();
    view.animate({
        duration: 700,
        center: [x, y]
      });
}

/**
 * Zoom In/Out to location
 * @param  {ol.Map} map
 * @param  {float} x
 * @param  {flaot} y
 * @param  {integer} value
 * @return {integer}
 */
function zoomToLocation(map, x, y, value) {
    var view = map.getView();
    view.animate({
        duration: 700,
        center: [x, y],
        zoom: view.getZoom() + value
      });
}
