/**
 * Create the map object
 * @param  {float} longitude
 * @param  {float} latitude
 * @param  {float} defaultMapZoom
 * @param  {string} idDiv
 * @return {ol.Map}
 */
function createMap(longitude, latitude, defaultMapZoom, idDiv){

    var mapCenter = ol.proj.transform([longitude, latitude], 'EPSG:4326', 'EPSG:3857');

    var view = createView(mapCenter, defaultMapZoom);

    var map = new ol.Map({
        target: idDiv,
        layers: [],
        controls : ol.control.defaults({
            zoom : false,
        }),
        view: view
    });

    var mousePosition = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(6),
        projection: 'EPSG:4326',
        className: 'coord',
        undefinedHTML: '&nbsp;'
      });

    map.addControl(mousePosition);
    map.addControl(new ol.control.Zoom({
        className: 'custom-zoom'
    }));

    $('#map').data('map', map);

    return map;
}

/**
 * Create source for a vector layer
 * @param  {array} features
 * @return {ol.source.Vector}
 */
function generateVectorSource(features, sourceName){
    var source = new ol.source.Vector({
        features: features
    });
    source.set('name', sourceName);
    return source;
}

/**
 * Create a vector layer from a source
 * @param  {ol.source.Vector} source
 * @return {ol.layer.Vector}
 */
function createVectorLayerFromSource(source){
    var layer = new ol.layer.Vector({
        source: source,
        renderBuffer: 200
    });
    return layer;
}

/**
 * Create View for a map
 * @param  {ol.Coordinate} mapCenter
 * @param  {number} defaultMapZoom
 * @param  {number} maxZoom
 * @param  {number} minZoom
 * @return {ol.View}
 */
function createView(mapCenter, defaultMapZoom) {
    //Create a View, set it center and zoom level
    var view = new ol.View({
        center:  mapCenter,
        zoom:    defaultMapZoom
    });

    return view;
}

/**
 * Create a vector layer with features
 * @param  {array} features
 * @param  {string} name
 * @return {ol.layer.Vector}
 */
function createLayer(features, layerName, sourceName) {
    var source = generateVectorSource(features, sourceName);
    var layer = createVectorLayerFromSource(source);
    layer.set('name', layerName);

    return layer;
}

/**
 * Add OpenStreetMap layer to the map
 * @param {ol.Map} map
 */
function addOsmLayerToMap(map) {
    var osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        crossOrigin: null
    });
    osmLayer.set('name', 'OSM');
    map.addLayer(osmLayer);
}

/**
 * Initialise the map
 * @param  {float} latitude
 * @param  {float} longitude
 * @param  {float} mapZoom
 * @param  {string} idDiv
 * @return {ol.Map}
 */
function initializeMap(latitude, longitude, mapZoom, idDiv) {
    var map = createMap(longitude, latitude, mapZoom, idDiv);
    addOsmLayerToMap(map);

    return map;
}
