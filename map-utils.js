/**
 * Get the layer that has the property name set to layerName
 * @param  {ol.Map} map
 * @param  {string} layerName
 * @return {ol.layer.Vector} | {bool}
 */
function getLayerByName(map, layerName) {
    var searchedLayer = false;
    map.getLayers().forEach(function(layer, i) {
        if (!hasProperty(layer, "name")) {
            throw "The layer doesn't have the property 'name'";
        }
        if (layer.get('name') == layerName) {
            searchedLayer = layer;
        }
    });
    return searchedLayer;
}

/**
 * Get the source of a map layer that is identified by its name
 * @param  {ol.Map} map
 * @param  {string} layerName
 * @return {ol.source.Vector} | {bool}
 */
function getSourceLayerByName(map, layerName) {
    var searchedLayer = getLayerByName(map, layerName);
    if (!searchedLayer) {
        return false;
    }
    return searchedLayer.getSource();
}

/**
 * Check if an object has a property in his properties
 * @param  {ol.Object}  object
 * @param  {string}  propertyName
 * @return {Boolean}
 */
function hasProperty(object, propertyName)  {
    var properties = object.getProperties();
    if (properties.hasOwnProperty(propertyName)) {
        return true;
    }

    return false;
}
