/**
 * Listen to map events
 * @param  {ol.Map} map
 * @param  {string} layerName
 * @param  {string} idPopover
 * @return {undefined}
 */
function initializeEvents(map, layerName, idPopover) {
    map.on('click', function (evt) {
        // getPointInfoThroughPopover(map, evt, layerName, idPopover, [0,-65]);
    });

    map.on('pointermove', function (e) {
      if (e.dragging) return;

      var pixel = map.getEventPixel(e.originalEvent);
      var hit = map.hasFeatureAtPixel(pixel);

      map.getTargetElement().style.cursor = hit ? 'pointer' : '';
    });
}
