# OL-UTILS
Functions for OpenLayers library to make the creation and interaction with the map easier

## Authors
Radu Teodor Ianache

## License
This project is licensed under the MIT License
