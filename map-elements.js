/**
 * Create an OpenLayer feature
 * @param  {float} latitude
 * @param  {float} longitude
 * @param  {string} info
 * @param  {string} title
 * @return {ol.Feature}
 */
function createOlFeature(latitude, longitude, info, title) {
    var olFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([longitude, latitude], 'EPSG:4326', 'EPSG:3857')),
        info: info,
        title: title,
        longitude: longitude,
        latitude: latitude
    });

    return olFeature;
}

/**
 * Configure the icon style for a feature
 * @param  {ol.Feature} olFeature
 * @param  {string} iconSrc
 * @param  {float} iconScale
 * @param  {float} anchorX
 * @param  {float} anchorY
 * @param  {integer} currentZIndex
 */
function configureOlFeature(olFeature, iconSrc, iconScale, anchorX, anchorY, currentZIndex) {
    var iconStyle = createIconStyle(iconSrc, iconScale, anchorX, anchorY, currentZIndex);
    olFeature.setStyle(iconStyle);
}

/**
 * Create style for feature icon
 * @param  {string} src
 * @param  {float} scale
 * @param  {float} anchorX
 * @param  {float} anchorY
 * @param  {integer} currentZIndex
 * @return {ol.style.Style}
 */
function createIconStyle(src, scale, anchorX, anchorY, currentZIndex) {
    var iconStyle = new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
                anchor: [anchorX, anchorY],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                scale: scale,
                src: src,
            })),
            zIndex: currentZIndex
        });
    return iconStyle;
}

/**
 * Add an overlay element to a map
 * @param {ol.Map} map
 * @param {string} idDivElement
 * @param {array} offset
 * @return {ol.Overlay}
 */
function addOverlayElemOnMap(map, idDivElement, offset){
    var divElement = $('#' + idDivElement)[0];

    var overlayElement = new ol.Overlay({
        element: divElement,
        positioning: 'top-center',
        offset: offset,
        stopEvent: false
    });
    map.addOverlay(overlayElement);
    return overlayElement;
}

// /**
//  * Creates popup element
//  * @param  {ol.Map} map
//  * @param  {ol.Coordinate} coord
//  * @param  {string} info
//  * @param  {string} title
//  * @param  {string} headerClass
//  * @param  {ol.Overlay} popoverElementOverlay
//  * @param  {string} idPopoverElement
//  * @return {object}
//  */
// function createPopoverElement(map, idPopoverElement, template){
//     var popoverElement = $("#"+idPopoverElement);
//     popoverElement.popover({
//         'placement': 'top',
//         'html': true,
//         'template': template
//     });
//     return popoverElement;
// }

// /**
//  * Configure bootstrap popover
//  * @param  {ol.Map} map
//  * @param  {string} idPopoverElement
//  * @param  {ol.Coordinate} coord
//  * @param  {array} offset
//  * @param  {jQuery} popoverElement
//  * @param  {string} info
//  * @param  {string} title
//  */
// function configurePopover(map, idPopoverElement, coord, offset, popoverElement, info, title) {
//     var overlayElement = addOverlayElemOnMap(map, idPopoverElement, offset);
//     overlayElement.setPosition(coord);
//
//     popoverElement.data('bs.popover').options.content = info;
//     popoverElement.data('bs.popover').options.title = title;
// }
